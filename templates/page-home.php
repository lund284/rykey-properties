<?php /* Template Name: Home Page */

get_header(); ?>

	<div id="content" class="home-page">
		<?php
			get_template_part( 'parts/home', 'hero' );
		?>
				
	</div> <!-- end #content -->

<?php get_footer(); ?>