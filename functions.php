<?php // Please keep this file clean, place any custom php snippets in custom-functions.php

require_once(get_stylesheet_directory().'/assets/functions/custom-enqueue-scripts.php');

require_once(get_stylesheet_directory().'/assets/functions/custom-functions.php');

// We need to find a better solution to WPAS, it's only been a pain to work with for anything other than simple posts searching.
// require_once(get_template_directory().'/wp-advanced-search/wpas.php');
// require_once(get_template_directory().'/assets/functions/wpas-config.php');

?>